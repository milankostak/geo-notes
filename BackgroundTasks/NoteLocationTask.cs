﻿using DataModel;
using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Windows.ApplicationModel.Background;
using Windows.Data.Xml.Dom;
using Windows.Devices.Geolocation.Geofencing;
using Windows.UI.Notifications;
using Windows.UI.Popups;

namespace BackgroundTasks
{
    public sealed class NoteLocationTask : IBackgroundTask
    {
        static readonly string TaskName = "LocationTask";

        public void Run(IBackgroundTaskInstance taskInstance)
        {
            var deferral = taskInstance.GetDeferral();
            DatabaseHelper.LoadConnection();
            Item.MarkAllInactive();

            var reports = GeofenceMonitor.Current.ReadReports();
            foreach (var report in reports)
            {
                List<Item> items = Item.SelectAllByPlaceId(Convert.ToInt64(report.Geofence.Id));
                foreach (var item in items)
                {
                    if (item.NotifyingByGeofence(report.NewState))
                    {
                        CreateToast(item);
                        item.Active = true;
                        item.Update();
                    }
                }
            }

            // mockData();

            TilesHelper.UpdateTile();

            deferral.Complete();
        }

        private void mockData()
        {
            List<Item> items2 = Item.SelectAllByPlaceId(1);
            foreach (var item in items2)
            {
                if (item.NotifyingByGeofence(GeofenceState.Entered))
                {
                    CreateToast(item);
                    item.Active = true;
                    item.Update();
                }
            }
        }

        private void CreateToast(Item item)
        {
            ToastTemplateType toastTemplate = ToastTemplateType.ToastText02;
            XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastTemplate);
            XmlNodeList textElements = toastXml.GetElementsByTagName("text");
            textElements[0].AppendChild(toastXml.CreateTextNode(item.Title));
            textElements[1].AppendChild(toastXml.CreateTextNode(item.Place.Name));
            ToastNotification toast = new ToastNotification(toastXml);

            var toastNavigationUriString = "ItemPage;" + item.Id;
            var toastElement = ((XmlElement)toastXml.SelectSingleNode("/toast"));
            toastElement.SetAttribute("launch", toastNavigationUriString);

            ToastNotificationManager.CreateToastNotifier().Show(new ToastNotification(toastXml));
        }

        public async static void Register()
        {
            if (!IsTaskRegistered())
            {
                var result = await BackgroundExecutionManager.RequestAccessAsync();
                BackgroundTaskBuilder geofenceTaskBuilder = new BackgroundTaskBuilder();

                geofenceTaskBuilder.Name = TaskName;
                geofenceTaskBuilder.TaskEntryPoint = typeof(NoteLocationTask).FullName;
                geofenceTaskBuilder.SetTrigger(new LocationTrigger(LocationTriggerType.Geofence));

                BackgroundTaskRegistration geofenceTask = geofenceTaskBuilder.Register();

                await (new MessageDialog("Location task registered")).ShowAsync();
            }
        }

        public static void Unregister()
        {
            var entry = BackgroundTaskRegistration.AllTasks.FirstOrDefault(kvp => kvp.Value.Name == TaskName);

            if (entry.Value != null)
                entry.Value.Unregister(true);
        }

        public static bool IsTaskRegistered()
        {
            var taskRegistered = false;
            var entry = BackgroundTaskRegistration.AllTasks.FirstOrDefault(kvp => kvp.Value.Name == TaskName);

            if (entry.Value != null)
                taskRegistered = true;

            return taskRegistered;
        }
    }
}
