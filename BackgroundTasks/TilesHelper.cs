﻿using DataModel.Model;
using System.Collections.Generic;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace BackgroundTasks
{
    public sealed class TilesHelper
    { 

        public static void UpdateTile()
        {
            TileUpdater updater = TileUpdateManager.CreateTileUpdaterForApplication();
            updater.EnableNotificationQueue(true);
            updater.Clear();

            List<Item> items = Item.SelectAllActive();

            int itemCount = 0;
            string imagePath = "ms-appx:///Assets/TileBadge.png";

            foreach (var item in items)
            {
                // BIG TILE
                XmlDocument bigTileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWide310x150IconWithBadgeAndText);
                XmlElement bigTileImage = bigTileXml.GetElementsByTagName("image")[0] as XmlElement;
                bigTileImage.SetAttribute("src", imagePath);
                bigTileXml.GetElementsByTagName("text")[0].InnerText = item.Title;
                bigTileXml.GetElementsByTagName("text")[1].InnerText = item.Place.Name;
                updater.Update(new TileNotification(bigTileXml));

                // Don't create more than 5 notifications
                if (itemCount++ > 5) break;
            }

            if (items.Count > 0)
            {
                // MEDIUM TILE
                XmlDocument mediumTileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquare150x150IconWithBadge);
                XmlElement tileImage = mediumTileXml.GetElementsByTagName("image")[0] as XmlElement;
                tileImage.SetAttribute("src", imagePath);
                updater.Update(new TileNotification(mediumTileXml));

                // SMALL TILE
                XmlDocument smallTileXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquare71x71IconWithBadge);
                tileImage = smallTileXml.GetElementsByTagName("image")[0] as XmlElement;
                tileImage.SetAttribute("src", imagePath);
                updater.Update(new TileNotification(smallTileXml));

                //badge update
                XmlDocument badgeXML = BadgeUpdateManager.GetTemplateContent(BadgeTemplateType.BadgeNumber);
                XmlElement badge = badgeXML.SelectSingleNode("/badge") as XmlElement;
                badge.SetAttribute("value", items.Count.ToString());
                BadgeNotification badgeNotification = new BadgeNotification(badgeXML);
                BadgeUpdateManager.CreateBadgeUpdaterForApplication().Update(badgeNotification);
            }
            else
            {
                BadgeUpdateManager.CreateBadgeUpdaterForApplication().Clear();
            }
        }

    }
}
