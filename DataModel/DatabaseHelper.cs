﻿using SQLitePCL;
using System;
using Windows.UI.Popups;

namespace DataModel
{
    static class Columns
    {
        public static string PlaceTable = "Place";
        public static string PlaceId = "Id";
        public static string PlaceLatitude = "Latitude";
        public static string PlaceLongitude = "Longitude";
        public static string PlaceName = "Name";
        public static string PlaceRadius = "Radius";
        public static string ItemTable = "Item";
        public static string ItemId = "Id";
        public static string ItemTitle = "Title";
        public static string ItemContent = "Content";
        public static string ItemOnEnter = "OnEnter";
        public static string ItemOnExit = "OnExit";
        public static string ItemStartTime = "StartTime";
        public static string ItemEndTime = "EndTime";
        public static string ItemDone = "Done";
        public static string ItemActive = "Active";
        public static string ItemPlaceId = "PlaceId";
    }

    public class DatabaseHelper
    {
        public static SQLiteConnection dbConnection;

        /// <summary>
        /// Executed when application is launched
        /// </summary>
        public static async void LoadDatabase()
        {
            try
            {
                LoadConnection();

                // create Place table
                string sql = @"CREATE TABLE IF NOT EXISTS `" + Columns.PlaceTable + @"` (
	                            `" + Columns.PlaceId + @"`			INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	                            `" + Columns.PlaceLatitude + @"`	NUMERIC NOT NULL,
	                            `" + Columns.PlaceLongitude + @"`	NUMERIC NOT NULL,
	                            `" + Columns.PlaceName + @"`		VARCHAR(30) NOT NULL,
	                            `" + Columns.PlaceRadius + @"`	    INTEGER NOT NULL );";
                using (var statement = dbConnection.Prepare(sql))
                {
                    statement.Step();
                }

                // create Item table
                sql = @"CREATE TABLE IF NOT EXISTS `" + Columns.ItemTable + @"` (
	                    `" + Columns.ItemId + @"`			INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	                    `" + Columns.ItemTitle + @"`		VARCHAR(30) NOT NULL,
	                    `" + Columns.ItemContent + @"`		VARCHAR(150) NOT NULL,
	                    `" + Columns.ItemOnEnter + @"`		BOOLEAN NOT NULL,
	                    `" + Columns.ItemOnExit + @"`		BOOLEAN NOT NULL,
	                    `" + Columns.ItemStartTime + @"`	DATETIME NOT NULL,
	                    `" + Columns.ItemEndTime + @"`		DATETIME NOT NULL,
	                    `" + Columns.ItemDone + @"`			BOOLEAN NOT NULL,
	                    `" + Columns.ItemActive + @"`		BOOLEAN NOT NULL,
	                    `" + Columns.ItemPlaceId + @"`		INTEGER NULL,
                        FOREIGN KEY(" + Columns.ItemPlaceId + ") REFERENCES " + Columns.PlaceTable + "(" + Columns.PlaceId + ") );";
                using (var statement = dbConnection.Prepare(sql))
                {
                    statement.Step();
                }
            }
            catch (Exception e)
            {
                await new MessageDialog("An error occured when loading data.\n" + e.Message, "Error").ShowAsync();
            }
        }

        public static void LoadConnection()
        {
            dbConnection = new SQLiteConnection("notes.db");

            // turn on foreign key constraints
            string sql = @"PRAGMA foreign_keys = ON;";

            using (var statement = dbConnection.Prepare(sql))
            {
                statement.Step();
            }
        }

    }
}
