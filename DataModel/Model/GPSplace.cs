﻿using SQLitePCL;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Resources;
using Windows.UI.Popups;

namespace DataModel.Model
{
    public class GPSplace
    {
        private readonly ResourceLoader resourceLoader = ResourceLoader.GetForCurrentView("Resources");
        public long Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Radius { get; set; }
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value.Trim();
                if (_name.Length > 30)
                {
                    //TODO vyhoď error
                    _name = _name.Substring(0, 30);
                }
            }
        }

        public GPSplace(double latitude, double longitude, string name, int radius)
        {
            Id = 0;
            Latitude = latitude;
            Longitude = longitude;
            Name = name;
            Radius = radius;
        }

        public GPSplace(long id, double latitude, double longitude, string name, int radius)
        {
            Id = id;
            Latitude = latitude;
            Longitude = longitude;
            Name = name;
            Radius = radius;
        }

        public GPSplace()
        {
            Id = 0;
            Latitude = 0;
            Longitude = 0;
            Radius = 0;
            Name = resourceLoader.GetString("PlaceNotSet");
        }

        public long Insert()
        {
            try
            {
                using (var statement = DatabaseHelper.dbConnection.Prepare(
                    "INSERT INTO " + Columns.PlaceTable + 
                        " (" + Columns.PlaceLatitude + ", " + Columns.PlaceLongitude + ", " + Columns.PlaceName + ", " + Columns.PlaceRadius + @")
                        VALUES (?, ?, ?, ?)"))
                {
                    statement.Bind(1, Latitude);
                    statement.Bind(2, Longitude);
                    statement.Bind(3, Name);
                    statement.Bind(4, Radius);
                    statement.Step();

                    Id = DatabaseHelper.dbConnection.LastInsertRowId();
                    return Id;
                }
            }
            catch (Exception e)
            {
                MessageDialog msgbox = new MessageDialog(
                    "An error occured when inserting " + Columns.PlaceTable + ".\n" + e.Message, "Error");
                msgbox.ShowAsync();
                return -1;
            }
        }

        public async void Update()
        {
            try
            {
                using (var statement = DatabaseHelper.dbConnection.Prepare(
                        "UPDATE " + Columns.PlaceTable + @"
                        SET " + Columns.PlaceLatitude + " = ?, " + Columns.PlaceLongitude + " = ?, "
                              + Columns.PlaceName + " = ?, " + Columns.PlaceRadius + @" = ?
                        WHERE " + Columns.PlaceId + " = ?"))
                {
                    statement.Bind(1, Latitude);
                    statement.Bind(2, Longitude);
                    statement.Bind(3, Name);
                    statement.Bind(4, Radius);
                    statement.Bind(5, Id);
                    statement.Step();
                }
            }
            catch (Exception e)
            {
                await new MessageDialog(
                    "An error occured when updating " + Columns.PlaceTable + " with " + Columns.PlaceId + "=" + Id + ".\n" + e.Message, "Error").ShowAsync();
            }
        }

        public static GPSplace SelectById(long id)
        {
            try
            {
                using (var statement = DatabaseHelper.dbConnection.Prepare(
                        "SELECT " + Columns.PlaceLatitude + ", " + Columns.PlaceLongitude + ", " + Columns.PlaceName + ", " + Columns.PlaceRadius + @"
                         FROM " + Columns.PlaceTable + @"
                         WHERE " + Columns.PlaceId + " = ?"))
                {
                    statement.Bind(1, id);
                    if (statement.Step() == SQLiteResult.ROW)
                    {
                        var latitude = (double)statement[0];
                        var longitude = (double)statement[1];
                        var name = (string)statement[2];
                        int radius = Convert.ToInt32((long)statement[3]);
                        GPSplace place = new GPSplace(id, latitude, longitude, name, radius);
                        return place;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                MessageDialog msgbox = new MessageDialog(
                    "An error occured when selecting " + Columns.PlaceTable + " with " + Columns.PlaceId + "=" + id + ".\n" + e.Message, "Error");
                msgbox.ShowAsync();
                return null;
            }
        }

        public static List<GPSplace> SelectAll()
        {
            try
            {
                using (var statement = DatabaseHelper.dbConnection.Prepare(
                        "SELECT " + Columns.PlaceId + ", " + Columns.PlaceLatitude + ", " + Columns.PlaceLongitude + ", "
                                  + Columns.PlaceName + ", " + Columns.PlaceRadius + @"
                         FROM " + Columns.PlaceTable))
                {
                    List<GPSplace> places = new List<GPSplace>();
                    while (statement.Step() == SQLiteResult.ROW)
                    {
                        var placeId = (long)statement[0];
                        var latitude = (double)statement[1];
                        var longitude = (double)statement[2];
                        var name = (string)statement[3];
                        int radius = Convert.ToInt32((long)statement[4]);
                        places.Add(new GPSplace(placeId, latitude, longitude, name, radius));
                    }
                    return places;
                }
            }
            catch (Exception e)
            {
                MessageDialog msgbox = new MessageDialog(
                    "An error occured when selecting all " + Columns.PlaceTable + "s.\n" + e.Message, "Error");
                msgbox.ShowAsync();
                return null;
            }
        }

        public async void Delete()
        {
            if (Item.SelectAllByPlaceId(Id).Count > 0)
            {
                string msg = resourceLoader.GetString("DeletePlaceWithNotesMsg");
                await new MessageDialog(msg, "Error").ShowAsync();
                return;
            }
            try
            {
                using (var statement = DatabaseHelper.dbConnection.Prepare(
                    "DELETE FROM " + Columns.PlaceTable + " WHERE " + Columns.PlaceId + " = ?"))
                {
                    statement.Bind(1, Id);
                    statement.Step();
                }
            }
            catch (Exception e)
            {
                await new MessageDialog(
                    "An error occured when deleting " + Columns.PlaceTable + " with " + Columns.PlaceId + "=" + Id + ".\n" + e.Message, "Error").ShowAsync();
            }
        }
    }
}
