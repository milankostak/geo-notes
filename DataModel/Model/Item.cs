﻿using SQLitePCL;
using System;
using System.Collections.Generic;
using Windows.Devices.Geolocation.Geofencing;
using Windows.UI.Popups;

namespace DataModel.Model
{
    public class Item
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public bool OnEnter { get; set; }
        public bool OnExit { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool Done { get; set; }
        public bool Active { get; set; }
        public GPSplace Place { get; set; }

        public bool Notifying
        {
            get
            {
                // Manage place
                if (Place.Id == 0)
                {
                    return false;
                }
                else if (!OnEnter && !OnExit)
                {
                    return false;
                }

                DateTime now = DateTime.UtcNow.ToLocalTime();
                // Manage start time
                if (DateTime.MinValue != StartTime && now < StartTime)
                {
                    return false;
                }
                // Manage end time
                if (DateTime.MinValue != EndTime && now > EndTime)
                {
                    return false;
                }
                if (Done)
                {
                    return false;
                }
                return true;
            }
        }

        public Item(string title, string content, bool onEnter, bool onExit, DateTime startTime, DateTime endTime, bool done, bool active, GPSplace place)
        {
            Id = 0;
            Title = title;
            Content = content;
            OnEnter = onEnter;
            OnExit = onExit;
            StartTime = startTime;
            EndTime = endTime;
            Done = done;
            Active = active;
            Place = place;
        }

        public Item(long id, string title, string content, bool onEnter, bool onExit, DateTime startTime, DateTime endTime, bool done, bool active, GPSplace place)
        {
            Id = id;
            Title = title;
            Content = content;
            OnEnter = onEnter;
            OnExit = onExit;
            StartTime = startTime;
            EndTime = endTime;
            Done = done;
            Active = active;
            Place = place;
        }

        public bool NotifyingByGeofence(GeofenceState state)
        {
            if (!Notifying)
            {
                return false;
            }
            else if (state == GeofenceState.Entered && OnEnter)
            {
                return true;
            }
            else if (state == GeofenceState.Exited && OnExit)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public long Insert()
        {
            try
            {
                using (var statement = DatabaseHelper.dbConnection.Prepare(@"
                    INSERT INTO " + Columns.ItemTable + @"
                        (" + Columns.ItemTitle + ", " + Columns.ItemContent + ", " + Columns.ItemOnEnter + ", " + Columns.ItemOnExit + @",
                         " + Columns.ItemStartTime + ", " + Columns.ItemEndTime + ", " + Columns.ItemDone + ", " + Columns.ItemActive + @",
                         " + Columns.ItemPlaceId + @")
                     VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"))
                {
                    statement.Bind(1, Title);
                    statement.Bind(2, Content);
                    statement.Bind(3, OnEnter ? 1 : 0);
                    statement.Bind(4, OnExit ? 1 : 0);
                    statement.Bind(5, StartTime.ToString());
                    statement.Bind(6, EndTime.ToString());
                    statement.Bind(7, Done ? 1 : 0);
                    statement.Bind(8, Active ? 1 : 0);
                    if (Place != null && Place.Id != 0)
                    {
                        statement.Bind(9, Place.Id);
                    }
                    else
                    {
                        statement.Bind(9, null);
                    }
                    statement.Step();

                    Id = DatabaseHelper.dbConnection.LastInsertRowId();
                    return Id;
                }
            }
            catch (Exception e)
            {
                MessageDialog msgbox = new MessageDialog("An error occured when inserting " + Columns.ItemTable + ".\n" + e.Message, "Error");
                msgbox.ShowAsync();
                return -1;
            }
        }

        public async void Update()
        {
            try
            {
                using (var statement = DatabaseHelper.dbConnection.Prepare(@"
                        UPDATE " + Columns.ItemTable + @"
                        SET " + Columns.ItemTitle + " = ?, " + Columns.ItemContent + " = ?, " + Columns.ItemOnEnter + " = ?, "
                              + Columns.ItemOnExit + " = ?, " + Columns.ItemStartTime + " = ?, " + Columns.ItemEndTime + " = ?, "
                              + Columns.ItemDone + " = ?, " + Columns.ItemActive + " = ?, " + Columns.ItemPlaceId + @" = ?
                        WHERE " + Columns.PlaceId + " = ?"))
                {
                    statement.Bind(1, Title);
                    statement.Bind(2, Content);
                    statement.Bind(3, OnEnter ? 1 : 0);
                    statement.Bind(4, OnExit ? 1 : 0);
                    statement.Bind(5, StartTime.ToString());
                    statement.Bind(6, EndTime.ToString());
                    statement.Bind(7, Done ? 1 : 0);
                    statement.Bind(8, Active ? 1 : 0);
                    if (Place != null && Place.Id != 0)
                    {
                        statement.Bind(9, Place.Id);
                    }
                    else
                    {
                        statement.Bind(9, null);
                    }

                    statement.Bind(10, Id);
                    statement.Step();
                }
            }
            catch (Exception e)
            {
                await new MessageDialog(
                    "An error occured when updating " + Columns.ItemTable + " with " + Columns.ItemId + "=" + Id + ".\n" + e.Message, "Error").ShowAsync();
            }
        }

        public static async void MarkAllInactive()
        {
            try
            {
                using (var statement = DatabaseHelper.dbConnection.Prepare(@"
                        UPDATE " + Columns.ItemTable + " SET " + Columns.ItemActive + " = 0"))
                {
                    statement.Step();
                }
            }
            catch (Exception e)
            {
                await new MessageDialog(
                    "An error occured when marking all " + Columns.PlaceTable + " as inactive.\n" + e.Message, "Error").ShowAsync();
            }
        }

        public static Item SelectById(long id)
        {
            try
            {
                using (var statement = DatabaseHelper.dbConnection.Prepare(@"
                        SELECT " + Columns.ItemId + ", " + Columns.ItemTitle + ", " + Columns.ItemContent + @", 
                                " + Columns.ItemOnEnter + ", " + Columns.ItemOnExit + ", " + Columns.ItemStartTime + @",
                                " + Columns.ItemEndTime + ", " + Columns.ItemDone + ", " + Columns.ItemActive + ", " + Columns.ItemPlaceId + @"
                        FROM " + Columns.ItemTable + @"
                        WHERE " + Columns.ItemId + " = ?"))
                {
                    statement.Bind(1, id);

                    if (statement.Step() == SQLiteResult.ROW)
                    {
                        var idItem = (long)statement[0];
                        var title = (string)statement[1];
                        var content = (string)statement[2];
                        bool onEnter = Convert.ToBoolean(statement[3]);
                        bool onExit = Convert.ToBoolean(statement[4]);
                        var _startTime = (string)statement[5];
                        var startTime = DateTime.Parse(_startTime);
                        var _endTime = (string)statement[6];
                        var endTime = DateTime.Parse(_endTime);
                        bool done = Convert.ToBoolean(statement[7]);
                        bool active = Convert.ToBoolean(statement[8]);
                        var temp = statement[9];
                        GPSplace place;
                        if (temp != null)
                        {
                            long placeId = (long)temp;
                            place = GPSplace.SelectById(placeId);
                        }
                        else
                        {
                            place = new GPSplace();
                        }
                        Item item = new Item(idItem, title, content, onEnter, onExit, startTime, endTime, done, active, place);
                        return item;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                MessageDialog msgbox = new MessageDialog(
                    "An error occured when selecting " + Columns.ItemTable + " with " + Columns.ItemId + "=" + id + ".\n" + e.Message, "Error");
                msgbox.ShowAsync();
                return null;
            }
        }

        private static List<Item> SelectAllPrivate(string where)
        {
            try
            {
                using (var statement = DatabaseHelper.dbConnection.Prepare(@"
                            SELECT I." + Columns.ItemId + ", I." + Columns.ItemTitle + ", I." + Columns.ItemContent + @",
                                   I." + Columns.ItemOnEnter + ", I." + Columns.ItemOnExit + ", I." + Columns.ItemStartTime + @",
                                   I." + Columns.ItemEndTime + ", I." + Columns.ItemDone + ", I." + Columns.ItemActive + @",
                                   I." + Columns.ItemPlaceId + ", P." + Columns.PlaceLatitude + ", P." + Columns.PlaceLongitude + @",
                                   P." + Columns.PlaceName + ", P." + Columns.PlaceRadius + @"
                            FROM " + Columns.ItemTable + @" I
                            LEFT OUTER JOIN " + Columns.PlaceTable + " P ON I." + Columns.ItemPlaceId + " = P." + Columns.PlaceId
                            + " " + where)
                      )
                {
                    List<Item> items = new List<Item>();
                    while (statement.Step() == SQLiteResult.ROW)
                    {
                        var idItem = (long)statement[0];
                        var title = (string)statement[1];
                        var content = (string)statement[2];
                        bool onEnter = Convert.ToBoolean(statement[3]);
                        bool onExit = Convert.ToBoolean(statement[4]);
                        var _startTime = (string)statement[5];
                        var startTime = DateTime.Parse(_startTime);
                        var _endTime = (string)statement[6];
                        var endTime = DateTime.Parse(_endTime);
                        bool done = Convert.ToBoolean(statement[7]);
                        bool active = Convert.ToBoolean(statement[8]);
                        GPSplace place;
                        if (statement[9] != null)
                        {
                            var placeId = (long)statement[9];
                            var latitude = (double)statement[10];
                            var longitude = (double)statement[11];
                            var name = (string)statement[12];
                            int radius = Convert.ToInt32((long)statement[13]);
                            place = new GPSplace(placeId, latitude, longitude, name, radius);
                        }
                        else
                        {
                            place = new GPSplace();
                        }
                        items.Add(new Item(idItem, title, content, onEnter, onExit, startTime, endTime, done, active, place));
                    }
                    return items;
                }
            }
            catch (Exception e)
            {
                MessageDialog msgbox = new MessageDialog(
                    "An error occured when selecting all " + Columns.ItemTable + "s.\n" + e.Message, "Error");
                msgbox.ShowAsync();
                return null;
            }
        }

        public static List<Item> SelectAll()
        {
            return SelectAllPrivate("");
        }

        public static List<Item> SelectAllActive()
        {
            return SelectAllPrivate("WHERE I." + Columns.ItemActive + " = 1");
        }

        public static List<Item> SelectAllByPlaceId(long placeId)
        {
            try
            {
                using (var statement = DatabaseHelper.dbConnection.Prepare(@"
                            SELECT I." + Columns.ItemId + ", I." + Columns.ItemTitle + ", I." + Columns.ItemContent + @",
                                   I." + Columns.ItemOnEnter + ", I." + Columns.ItemOnExit + ", I." + Columns.ItemStartTime + @",
                                   I." + Columns.ItemEndTime + ", I." + Columns.ItemDone + ", I." + Columns.ItemActive + @",
                                   P." + Columns.PlaceLatitude + ", P." + Columns.PlaceLongitude + ", P." + Columns.PlaceName + @",
                                   P." + Columns.PlaceRadius + @"
                            FROM " + Columns.ItemTable + @" I
                            JOIN " + Columns.PlaceTable + " P ON I." + Columns.ItemPlaceId + " = P." + Columns.PlaceId + @"
                            WHERE I." + Columns.ItemPlaceId + " = ?")
                      )
                {
                    statement.Bind(1, placeId);
                    List<Item> items = new List<Item>();
                    while (statement.Step() == SQLiteResult.ROW)
                    {
                        var idItem = (long)statement[0];
                        var title = (string)statement[1];
                        var content = (string)statement[2];
                        bool onEnter = Convert.ToBoolean(statement[3]);
                        bool onExit = Convert.ToBoolean(statement[4]);
                        var _startTime = (string)statement[5];
                        var startTime = DateTime.Parse(_startTime);
                        var _endTime = (string)statement[6];
                        var endTime = DateTime.Parse(_endTime);
                        bool done = Convert.ToBoolean(statement[7]);
                        bool active = Convert.ToBoolean(statement[8]);
                        var latitude = (double)statement[9];
                        var longitude = (double)statement[10];
                        var name = (string)statement[11];
                        int radius = Convert.ToInt32((long)statement[12]);
                        GPSplace place = new GPSplace(placeId, latitude, longitude, name, radius);
                        items.Add(new Item(idItem, title, content, onEnter, onExit, startTime, endTime, done, active, place));
                    }
                    return items;
                }
            }
            catch (Exception e)
            {
                MessageDialog msgbox = new MessageDialog(
                    "An error occured when selecting all " + Columns.ItemTable + "s.\n" + e.Message, "Error");
                msgbox.ShowAsync();
                return null;
            }
        }

        public async void Delete()
        {
            try
            {
                using (var statement = DatabaseHelper.dbConnection.Prepare(
                    "DELETE FROM " + Columns.ItemTable + " WHERE " + Columns.ItemId + " = ?"))
                {
                    statement.Bind(1, Id);
                    statement.Step();
                }
            }
            catch (Exception e)
            {
                await new MessageDialog(
                    "An error occured when deleting " + Columns.ItemTable + " with " + Columns.ItemId + "=" + Id + ".\n" + e.Message, "Error").ShowAsync();
            }
        }

        public void Dismiss()
        {
            Active = false;
            Update();
        }

        public void MarkDone()
        {
            Done = true;
            if (Active) Dismiss();
            else Update();
        }
    }
}
