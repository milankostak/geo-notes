## What is this project about ##

This is a school project made during summer semester in 2016. It was done for "Introduction to Mobile Technology" and "Smart Approaches to IS and Application Creating" subjects. The main goal of the application is to allow user to create notes not just with time but with possibility to interact with location.

## Basic usage scenario ##

Basic usage scenario includes user creating a note and assigning it to a predefined location. The app would then notify user when he enters/exits the given area. Notes have many other settings possibilities.

## Platform ##

Target platform of this project is Windows Phone 8.1.