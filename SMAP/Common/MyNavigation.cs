﻿using DataModel.Model;
using System;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml.Controls;

namespace SMAP.Common
{
    public class MyNavigation
    {
        private readonly ResourceLoader resourceLoader = ResourceLoader.GetForCurrentView("Resources");
        private Frame Frame;

        public MyNavigation(Frame Frame)
        {
            this.Frame = Frame;
        }

        public void Navigate(Type page)
        {
            if (!Frame.Navigate(page))
            {
                throw new Exception(resourceLoader.GetString("NavigationFailedExceptionMessage"));
            }
        }

        public void Navigate(Type page, object param)
        {
            if (!Frame.Navigate(page, param))
            {
                throw new Exception(resourceLoader.GetString("NavigationFailedExceptionMessage"));
            }
        }

        public void Navigate(Item item, Type type)
        {
            if (item != null)
            {
                Navigate(type, item.Id);
            }
        }

        public void Navigate(GPSplace place)
        {
            if (place != null)
            {
                Navigate(typeof(PlacePage), place.Id);
            }
        }

    }
}
