﻿using DataModel.Model;
using System;
using System.Linq;
using Windows.Devices.Geolocation;
using Windows.Devices.Geolocation.Geofencing;

namespace SMAP.Common
{
    class GeofencesHelper
    {

        public static void CreateGeofence(GPSplace place)
        {
            //if already registered then return
            string id = place.Id.ToString();
            if (GeofenceMonitor.Current.Geofences.SingleOrDefault(g => g.Id == id) != null) return;

            BasicGeoposition position = new BasicGeoposition();
            position.Latitude = place.Latitude;
            position.Longitude = place.Longitude;

            Geocircle geocircle = new Geocircle(position, place.Radius);

            MonitoredGeofenceStates mask =
                MonitoredGeofenceStates.Entered
                | MonitoredGeofenceStates.Exited;

            Geofence geofence = new Geofence(id, geocircle, mask, false, TimeSpan.FromSeconds(10));
            GeofenceMonitor.Current.Geofences.Add(geofence);
        }

        public static void RemoveGeofence(GPSplace place)
        {
            string id = place.Id.ToString();
            var geofence = GeofenceMonitor.Current.Geofences.SingleOrDefault(g => g.Id == id);

            if (geofence != null)
                GeofenceMonitor.Current.Geofences.Remove(geofence);
        }
    }
}
