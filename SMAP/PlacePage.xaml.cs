﻿using SMAP.Common;
using System;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;
using DataModel.Model;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace SMAP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PlacePage : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private readonly ResourceLoader resourceLoader = ResourceLoader.GetForCurrentView("Resources");
        private readonly int BASIC_ZOOMLEVEL = 14;
        private readonly Color TAPPED_POSITION_COLOR = Colors.Blue;
        private readonly Color CURRENT_POSITION_COLOR = Colors.Orange;
        private Geopoint placePosition;
        private Geopoint currentPosition;

        private GPSplace place;

        public PlacePage()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            var param = e.NavigationParameter;
            if (param == null)
            {
                place = null;
                placePosition = null;
            }
            else
            {
                //editing existing place => allow deleting
                DeletePlaceAppBarButton.Visibility = Visibility.Visible;
                place = GPSplace.SelectById((long)param);
                PlaceName.Text = place.Name;
                RadiusComboBox.SelectedIndex = (place.Radius == 30) ? 0 : place.Radius / 50;

                BasicGeoposition position = new BasicGeoposition();
                position.Latitude = place.Latitude;
                position.Longitude = place.Longitude;
                placePosition = new Geopoint(position);
            }
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void placeMapControl_MapTapped(MapControl sender, MapInputEventArgs args)
        {
            placePosition = args.Location;
            Repaint();
        }

        private async void placeMapControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (placePosition == null)
            {
                await findCurrentPosition();
                placeMapControl.Center = currentPosition;
                placeMapControl.ZoomLevel = BASIC_ZOOMLEVEL;
            }
            else
            {
                placeMapControl.Center = placePosition;
                placeMapControl.ZoomLevel = BASIC_ZOOMLEVEL;
                Repaint();
                await findCurrentPosition();
            }
            Repaint();
        }

        private void MyPositionButton_Click(object sender, RoutedEventArgs e)
        {
            placeMapControl.Center = currentPosition;
            placeMapControl.ZoomLevel = BASIC_ZOOMLEVEL + 2;
        }

        private async void SavePlaceButton_Click(object sender, RoutedEventArgs e)
        {
            string name = PlaceName.Text;
            if (name == "" || placePosition == null)
            {
                string msgContent = resourceLoader.GetString("NewPlaceInsertNameAndPosition");
                string msgTitle = resourceLoader.GetString("MissingValues");
                await new MessageDialog(msgContent, msgTitle).ShowAsync();
            }
            else
            {
                string radiusString = ((ComboBoxItem)RadiusComboBox.SelectedItem).Content.ToString();
                int radius = Convert.ToInt32(radiusString);
                //creating
                if (place == null)
                {
                    GPSplace place = new GPSplace(placePosition.Position.Latitude, placePosition.Position.Longitude, name, radius);
                    long id = place.Insert();
                    GeofencesHelper.CreateGeofence(place);
                }
                //editing
                else
                {
                    GeofencesHelper.RemoveGeofence(place);
                    place.Name = name;
                    place.Latitude = placePosition.Position.Latitude;
                    place.Longitude = placePosition.Position.Longitude;
                    place.Radius = radius;
                    place.Update();
                    GeofencesHelper.CreateGeofence(place);
                }
                NavigationHelper.GoBack();
            }
        }

        private async void DeletePlaceButton_Click(object sender, RoutedEventArgs e)
        {
            string message = resourceLoader.GetString("PlacePageDeleteMessageContent");
            string title = resourceLoader.GetString("AreYouSure");
            MessageDialog msgbox = new MessageDialog(message, title);
            msgbox.Commands.Add(new UICommand(resourceLoader.GetString("yes"), new UICommandInvokedHandler(DeletePlace)));
            msgbox.Commands.Add(new UICommand(resourceLoader.GetString("no")));
            await msgbox.ShowAsync();
        }

        private void DeletePlace(IUICommand commandLabel)
        {
            GeofencesHelper.RemoveGeofence(place);
            place.Delete();
            NavigationHelper.GoBack();
        }

        private async Task findCurrentPosition()
        {
            Geolocator geolocator = new Geolocator();
            geolocator.DesiredAccuracyInMeters = 50;
            try
            {
                Geoposition geoposition = await geolocator.GetGeopositionAsync(
                    maximumAge: TimeSpan.FromMinutes(5),
                    timeout: TimeSpan.FromSeconds(7));

                currentPosition = geoposition.Coordinate.Point;
            }
            catch (UnauthorizedAccessException)
            {
                // the app does not have the right capability or the location master switch is off         
                //StatusTextBlock.Text = "location is disabled in phone settings.";
                currentPosition = null;
            }
        }

        private void Repaint()
        {
            placeMapControl.Children.Clear();

            if (placePosition != null)
            {
                Ellipse fence1 = new Ellipse();
                fence1.Width = 30;
                fence1.Height = 30;
                fence1.StrokeThickness = 2;
                fence1.Stroke = new SolidColorBrush(TAPPED_POSITION_COLOR);
                MapControl.SetLocation(fence1, placePosition);
                MapControl.SetNormalizedAnchorPoint(fence1, new Point(0.5, 0.5));
                placeMapControl.Children.Add(fence1);
            }
            
            if (currentPosition != null)
            {
                Ellipse fence2 = new Ellipse();
                fence2.Width = 30;
                fence2.Height = 30;
                fence2.StrokeThickness = 2;
                fence2.Stroke = new SolidColorBrush(CURRENT_POSITION_COLOR);
                MapControl.SetLocation(fence2, currentPosition);
                MapControl.SetNormalizedAnchorPoint(fence2, new Point(0.5, 0.5));
                placeMapControl.Children.Add(fence2);
            }
        }
    }
}
