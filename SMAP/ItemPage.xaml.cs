﻿using SMAP.Common;
using System;
using Windows.ApplicationModel.Resources;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using DataModel.Model;
using Windows.UI.Xaml.Media;
using Windows.UI;
using BackgroundTasks;

// The Pivot Application template is documented at http://go.microsoft.com/fwlink/?LinkID=391641

namespace SMAP
{
    /// <summary>
    /// A page that displays details for a single item within a group.
    /// </summary>
    public sealed partial class ItemPage : Page
    {
        private readonly NavigationHelper navigationHelper;
        private readonly ObservableDictionary defaultViewModel = new ObservableDictionary();
        private readonly ResourceLoader resourceLoader = ResourceLoader.GetForCurrentView("Resources");
        private MyNavigation navigation;
        private Item item;

        public ItemPage()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        } 

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation. Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>.
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            navigation = new MyNavigation(Frame);
            long id = Convert.ToInt64(e.NavigationParameter);
            item = Item.SelectById(id);
            DefaultViewModel["Item"] = item;

            // Manage place
            if (item.Place.Id != 0)
            {
                string yes = resourceLoader.GetString("yes");
                string no = resourceLoader.GetString("no");
                TextBlockOnEnter.Text = resourceLoader.GetString("NotifyOnEnter") + (item.OnEnter ? yes : no);
                TextBlockOnExit.Text = resourceLoader.GetString("NotifyOnExit") + (item.OnExit ? yes : no);
            }
            else
            {
                TextBlockOnEnter.Height = 0;
                TextBlockOnExit.Height = 0;
            }

            DateTime now = DateTime.UtcNow.ToLocalTime();
            // Manage start time
            if (item.StartTime == DateTime.MinValue)
            {
                TextBlockStartTime.Height = 0;
                TextBlockStartTimeHeader.Height = 0;
            }

            // Manage end time
            if (item.EndTime == DateTime.MinValue)
            {
                TextBlockEndTime.Height = 0;
                TextBlockEndTimeHeader.Height = 0;
            }

            // Mark active
            if (item.Active)
            {
                DismissNoteButton.Visibility = Visibility.Visible;
            }

            showNotifying();
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/>.</param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void EditNoteAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            navigation.Navigate(item, typeof(NewNote));
        }

        private async void DeleteNoteAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            string message = resourceLoader.GetString("ItemPageDeleteMessageContent");
            string title = resourceLoader.GetString("AreYouSure");
            MessageDialog msgbox = new MessageDialog(message, title);
            msgbox.Commands.Add(new UICommand(resourceLoader.GetString("yes"), new UICommandInvokedHandler(DeleteNote)));
            msgbox.Commands.Add(new UICommand(resourceLoader.GetString("no")));
            await msgbox.ShowAsync();
        }

        private void DeleteNote(IUICommand commandLabel)
        {
            item.Delete();
            NavigationHelper.GoBack();
        }

        private void cbMarkDone_Checked(object sender, RoutedEventArgs e)
        {
            bool active = item.Active;
            item.MarkDone();
            showNotifying();
            if (active)
            {
                DismissNote();
            }
        }

        private void cbMarkDone_Unchecked(object sender, RoutedEventArgs e)
        {
            item.Done = false;
            item.Update();
            showNotifying();
        }

        private void DismissNoteButton_Click(object sender, RoutedEventArgs e)
        {
            DismissNote();
        }

        private void showNotifying()
        {
            bool notify = item.Notifying;
            TextBlockCurrentNotify.Text = notify ? "Currently notifying" : "Currently not notifying";
            TextBlockCurrentNotify.Foreground = notify ? new SolidColorBrush(Colors.LightGreen) : new SolidColorBrush(Colors.LightCoral);
        }

        private void DismissNote()
        {
            item.Dismiss();
            DismissNoteButton.Visibility = Visibility.Collapsed;
            TilesHelper.UpdateTile();
        }
    }
}