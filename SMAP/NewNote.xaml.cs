﻿using SMAP.Common;
using System;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using DataModel;
using Windows.UI.Popups;
using Windows.ApplicationModel.Resources;
using DataModel.Model;
using System.Linq;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace SMAP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class NewNote : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private readonly ResourceLoader resourceLoader = ResourceLoader.GetForCurrentView("Resources");
        private Item item;

        public NewNote()
        {
            this.InitializeComponent();
            InitPlaceComboBox();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            if (e.NavigationParameter != null)
            {
                long id = Convert.ToInt64(e.NavigationParameter);
                LoadItemForEdit(id);
            }
            else
            {
                item = null;
            }
        }

        private void LoadItemForEdit(long id)
        {
            item = Item.SelectById(id);
            DefaultViewModel["Item"] = item;

            if (item.Place.Id != 0)
            {
                long id2 = item.Place.Id;
                GPSplace itemGPSplace = PlaceComboBox.Items.OfType<GPSplace>().SingleOrDefault(g => g.Id == id2);
                PlaceComboBox.SelectedItem = itemGPSplace;
            }
            else
            {
                PlaceComboBox.SelectedIndex = 0;
            }

            cbNotifyOnEnter.IsChecked = item.OnEnter;
            cbNotifyOnExit.IsChecked = item.OnExit;

            if (item.StartTime != DateTime.MinValue)
            {
                cbStartTime.IsChecked = true;
                NoteStartDatePicker.IsEnabled = true;
                NoteStartDatePicker.Date = item.StartTime.Date;
                NoteStartTimePicker.IsEnabled = true;
                NoteStartTimePicker.Time = item.StartTime.TimeOfDay;
            }

            if (item.EndTime != DateTime.MinValue)
            {
                cbEndTime.IsChecked = true;
                NoteEndDatePicker.IsEnabled = true;
                NoteEndDatePicker.Date = item.EndTime.Date;
                NoteEndTimePicker.IsEnabled = true;
                NoteEndTimePicker.Time = item.EndTime.TimeOfDay;
            }
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void InitPlaceComboBox()
        {
            List<GPSplace> places = new List<GPSplace>();
            places.Add(new GPSplace());
            places.AddRange(GPSplace.SelectAll());
            PlaceComboBox.DataContext = places;
        }

        private async void SaveNoteButton_Click(object sender, RoutedEventArgs e)
        {
            string title = NoteTitle.Text;
            string content = NoteText.Text;
            if (title == "" || content == "")
            {
                string msgContent = resourceLoader.GetString("NewNoteInsertTitleAndContent");
                string msgTitle = resourceLoader.GetString("MissingValues");
                await new MessageDialog(msgContent, msgTitle).ShowAsync();
            }
            else
            {
                GPSplace place = ((GPSplace)PlaceComboBox.SelectedItem);

                bool onEnter = cbNotifyOnEnter.IsChecked.HasValue ? (bool)cbNotifyOnEnter.IsChecked : false;
                bool onExit = cbNotifyOnExit.IsChecked.HasValue ? (bool)cbNotifyOnExit.IsChecked : false;

                // Start DateTime
                DateTime startDateTime = DateTime.MinValue;
                if ((bool)cbStartTime.IsChecked)
                {
                    DateTime tempStartDateTime = NoteStartDatePicker.Date.Date;
                    TimeSpan startTime = NoteStartTimePicker.Time;
                    startDateTime = tempStartDateTime.AddHours(startTime.TotalHours);
                }

                // End DateTime
                DateTime endDateTime = DateTime.MinValue;
                if ((bool)cbEndTime.IsChecked)
                {
                    DateTime tempEndDateTime = NoteEndDatePicker.Date.Date;
                    TimeSpan endTime = NoteEndTimePicker.Time;
                    endDateTime = tempEndDateTime.AddHours(endTime.TotalHours);
                }

                // Insert
                if (item == null)
                {
                    Item item = new Item(title, content, onEnter, onExit, startDateTime, endDateTime, false, false, place);
                    item.Insert();
                }
                // Update
                else
                {
                    item.Title = title;
                    item.Content = content;
                    item.OnEnter = onEnter;
                    item.OnExit = onExit;
                    item.StartTime = startDateTime;
                    item.EndTime = endDateTime;
                    item.Place = place;
                    item.Update();
                }
                NavigationHelper.GoBack();
            }
        }

        private void cbAdvancedOptions_Checked(object sender, RoutedEventArgs e)
        {
            cbNotifyOnEnter.Visibility = Visibility.Visible;
            cbNotifyOnExit.Visibility = Visibility.Visible;
            StartTimeGrid.Visibility = Visibility.Visible;
            EndTimeGrid.Visibility = Visibility.Visible;
        }

        private void cbAdvancedOptions_Unchecked(object sender, RoutedEventArgs e)
        {
            cbNotifyOnEnter.Visibility = Visibility.Collapsed;
            cbNotifyOnExit.Visibility = Visibility.Collapsed;
            StartTimeGrid.Visibility = Visibility.Collapsed;
            EndTimeGrid.Visibility = Visibility.Collapsed;
        }

        private void cbStartTime_Checked(object sender, RoutedEventArgs e)
        {
            NoteStartDatePicker.IsEnabled = true;
            NoteStartTimePicker.IsEnabled = true;
        }

        private void cbStartTime_Unchecked(object sender, RoutedEventArgs e)
        {
            NoteStartDatePicker.IsEnabled = false;
            NoteStartTimePicker.IsEnabled = false;
        }

        private void cbEndTime_Checked(object sender, RoutedEventArgs e)
        {
            NoteEndDatePicker.IsEnabled = true;
            NoteEndTimePicker.IsEnabled = true;
        }

        private void cbEndTime_Unchecked(object sender, RoutedEventArgs e)
        {
            NoteEndDatePicker.IsEnabled = false;
            NoteEndTimePicker.IsEnabled = false;
        }
    }
}
