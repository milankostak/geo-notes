﻿using BackgroundTasks;
using SMAP.Common;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Resources;
using Windows.Devices.Geolocation.Geofencing;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using DataModel.Model;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Controls.Primitives;

// The Pivot Application template is documented at http://go.microsoft.com/fwlink/?LinkID=391641

namespace SMAP
{
    public sealed partial class PivotPage : Page
    {
        private readonly NavigationHelper navigationHelper;
        private readonly ObservableDictionary defaultViewModel = new ObservableDictionary();
        private readonly ResourceLoader resourceLoader = ResourceLoader.GetForCurrentView("Resources");
        private MyNavigation navigation;

        public PivotPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;

            NoteLocationTask.Register();
        }

        #region NavigationHelper
        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation. Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>.
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session. The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            navigation = new MyNavigation(Frame);
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache. Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/>.</param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #endregion

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        #region Buttons registration

        private void CreateNoteAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            navigation.Navigate(typeof(NewNote));
        }

        private void CreatePlaceAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            navigation.Navigate(typeof(PlacePage));
        }

        private void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            navigation.Navigate(typeof(AboutPage));
        }

        private void DismissAllButton_Click(object sender, RoutedEventArgs e)
        {
            Item.MarkAllInactive();
            SetAllRelevantNotes();
            TilesHelper.UpdateTile();
        }

        #endregion

        #region Pivot list views

        private void ListViewAllNotes_ItemClick(object sender, ItemClickEventArgs e)
        {
            NavigateToItem(e);
        }

        private void ListViewAllNotes_Loaded(object sender, RoutedEventArgs e)
        {
            SetAllNotes();
        }

        private void SetAllNotes()
        {
            List<Item> items = Item.SelectAll();
            NumberOfNotesValue.Text = items.Count.ToString();
            ListViewAllNotes.ItemsSource = items;
        }

        private void ListViewRelevantNotes_ItemClick(object sender, ItemClickEventArgs e)
        {
            NavigateToItem(e);
        }

        private void ListViewRelevantNotes_Loaded(object sender, RoutedEventArgs e)
        {
            SetAllRelevantNotes();
        }

        private void SetAllRelevantNotes()
        {
            List<Item> items = Item.SelectAllActive();
            NumberOfRelevantNotesValue.Text = items.Count.ToString();
            if (items.Count > 0)
            {
                DismissAllButton.Visibility = Visibility.Visible;
            }
            else
            {
                DismissAllButton.Visibility = Visibility.Collapsed;
            }
            ListViewRelevantNotes.ItemsSource = items;
        }

        private void NavigateToItem(ItemClickEventArgs e)
        {
            Item item = ((Item)e.ClickedItem);
            navigation.Navigate(item, typeof(ItemPage));
        }

        private void ListViewAllPlaces_ItemClick(object sender, ItemClickEventArgs e)
        {
            GPSplace place = ((GPSplace)e.ClickedItem);
            navigation.Navigate(place);
        }

        private void ListViewAllPlaces_Loaded(object sender, RoutedEventArgs e)
        {
            SetAllPlaces();
        }

        private void SetAllPlaces()
        {
            List<GPSplace> places = GPSplace.SelectAll();
            NumberOfPlacesValue.Text = places.Count.ToString();
            ListViewAllPlaces.ItemsSource = places;
        }

        #endregion

        #region Pivot list views - Flyout menus

        private void ListViewAllNotes_ItemHolding(object sender, HoldingRoutedEventArgs e)
        {
            FrameworkElement senderElement = sender as FrameworkElement;
            FlyoutBase flyoutBase = FlyoutBase.GetAttachedFlyout(senderElement);
            flyoutBase.ShowAt(senderElement);
        }

        private void MenuFlyoutItemEdit_Click(object sender, RoutedEventArgs e)
        {
            Item item = GetItemFromFlyout(sender);
            navigation.Navigate(item, typeof(NewNote));
        }

        private async void MenuFlyoutItemDelete_Click(object sender, RoutedEventArgs e)
        {
            Item item = GetItemFromFlyout(sender);
            if (item != null)
            {
                string message = resourceLoader.GetString("ItemPageDeleteMessageContent");
                string title = resourceLoader.GetString("AreYouSure");
                MessageDialog msgbox = new MessageDialog(message, title);
                msgbox.Commands.Add(new UICommand(resourceLoader.GetString("yes"), new UICommandInvokedHandler(DeleteItem), item.Id));
                msgbox.Commands.Add(new UICommand(resourceLoader.GetString("no")));
                await msgbox.ShowAsync();
            }
        }

        private void DeleteItem(IUICommand commandLabel)
        {
            long id = Convert.ToInt64(commandLabel.Id);
            Item item = Item.SelectById(id);
            bool active = item.Active;
            item.Delete();
            SetAllNotes();
            SetAllRelevantNotes();
            if (active)
            {
                TilesHelper.UpdateTile();
            }
        }

        private Item GetItemFromFlyout(object sender)
        {
            MenuFlyoutItem menuFlyoutItem = sender as MenuFlyoutItem;
            if (menuFlyoutItem != null)
            {
                Item item = menuFlyoutItem.DataContext as Item;
                if (item != null)
                {
                    return item;
                }
                return null;
            }
            return null;
        }

        private void ListViewRelevantNotes_ItemHolding(object sender, HoldingRoutedEventArgs e)
        {
            FrameworkElement senderElement = sender as FrameworkElement;
            FlyoutBase flyoutBase = FlyoutBase.GetAttachedFlyout(senderElement);
            flyoutBase.ShowAt(senderElement);
        }

        private void MenuFlyoutItemDone_Click(object sender, RoutedEventArgs e)
        {
            Item item = GetItemFromFlyout(sender);
            if (item != null)
            {
                item.MarkDone();
                // it is shown on relevant notes, so it has to be in active state => always update tile
                SetAllRelevantNotes();
                TilesHelper.UpdateTile();
            }
        }

        private void MenuFlyoutItemDismiss_Click(object sender, RoutedEventArgs e)
        {
            Item item = GetItemFromFlyout(sender);
            if (item != null)
            {
                item.Dismiss();
                SetAllRelevantNotes();
                TilesHelper.UpdateTile();
            }
        }

        private void ListViewAllPlaces_ItemHolding(object sender, HoldingRoutedEventArgs e)
        {
            FrameworkElement senderElement = sender as FrameworkElement;
            FlyoutBase flyoutBase = FlyoutBase.GetAttachedFlyout(senderElement);
            flyoutBase.ShowAt(senderElement);
        }

        private void MenuFlyoutPlaceEdit_Click(object sender, RoutedEventArgs e)
        {
            GPSplace place = GetPlaceFromFlyout(sender);
            navigation.Navigate(place);
        }

        private async void MenuFlyoutPlaceDelete_Click(object sender, RoutedEventArgs e)
        {
            GPSplace place = GetPlaceFromFlyout(sender);
            if (place != null)
            {
                string message = resourceLoader.GetString("PlacePageDeleteMessageContent");
                string title = resourceLoader.GetString("AreYouSure");
                MessageDialog msgbox = new MessageDialog(message, title);
                msgbox.Commands.Add(new UICommand(resourceLoader.GetString("yes"), new UICommandInvokedHandler(DeletePlace), place.Id));
                msgbox.Commands.Add(new UICommand(resourceLoader.GetString("no")));
                await msgbox.ShowAsync();
            }
        }

        private void DeletePlace(IUICommand commandLabel)
        {
            long id = Convert.ToInt64(commandLabel.Id);
            GPSplace place = GPSplace.SelectById(id);
            GeofencesHelper.RemoveGeofence(place);
            place.Delete();
            SetAllPlaces();
        }

        private GPSplace GetPlaceFromFlyout(object sender)
        {
            MenuFlyoutItem menuFlyoutItem = sender as MenuFlyoutItem;
            if (menuFlyoutItem != null)
            {
                GPSplace place = menuFlyoutItem.DataContext as GPSplace;
                if (place != null)
                {
                    return place;
                }
                return null;
            }
            return null;
        }

        #endregion

    }
}
